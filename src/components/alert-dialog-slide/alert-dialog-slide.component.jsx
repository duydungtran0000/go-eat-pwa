import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";
import { setConnectivity } from "../../redux/connectivity/connectivity.actions";
// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  alert: {
    fontSize: "14pt",
    fontWeight: "bold",
  },
}));

const AlertDialogSlide = ({ children, status, setConnectivity }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(true);

  useEffect(() => {
    status ? setConnectivity("online") : setConnectivity("offline");
  });

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      {status ? (
        <Snackbar open={open} autoHideDuration={4000} onClose={handleClose}>
          <MuiAlert
            className={classes.alert}
            onClose={handleClose}
            severity="success"
          >
            {children}
          </MuiAlert>
        </Snackbar>
      ) : (
        <Snackbar open={open} onClose={handleClose}>
          <MuiAlert
            className={classes.alert}
            onClose={handleClose}
            severity="warning"
          >
            {children}
          </MuiAlert>
        </Snackbar>
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setConnectivity: (isConnecting) => dispatch(setConnectivity(isConnecting)),
});

export default connect(null, mapDispatchToProps)(AlertDialogSlide);
