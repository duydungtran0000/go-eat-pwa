import React from "react";
import "./menu-item.styles.scss";
import { withRouter } from "react-router-dom";
import Image from 'react-image-webp';

// import { LazyLoadImage } from "react-lazy-load-image-component";
// <LazyLoadImage
// className="background-image"
// alt=""
// effect="opacity"
// src={imageUrl}
// width="100%"
// height="100%"
// />
//  <div alt="" className="background-image" style={{backgroundImage: `url(${imageUrl})`}}>
const MenuItem = ({ title, imageUrl, size, history, linkUrl, match }) => (
  <div
    className={`${size} menu-item`}
    onClick={() => history.push(`${match.url}${linkUrl}`)}
  >
  
    <Image className="background-image" alt="" webp={imageUrl}/>
    <div className="content">
      <div className="title">{title.toUpperCase()}</div>
      <span className="subtitle">ORDER NOW</span>
    </div>
  </div>
);

export default withRouter(MenuItem);
