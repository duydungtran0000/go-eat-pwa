import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import CollectionItem from '../../components/collection-item/collection-item.component';
import { selectCollection } from "../../redux/shop/shop.selectors";
import { selectSearchField } from "../../redux/search/search.selectors";
import './collection.styles.scss';

const CollectionPage = ({ collection, searchField }) => {
    const {title, items} = collection;
    return (
    <div className="collection-page">
      <h2 className='title'>{title}</h2>
      <div className='items'>
        {
            items
            .filter(item => item.establishment.toLowerCase().includes(searchField.toLowerCase()))
            .map(item =>
                <CollectionItem key={item.id} item={item}/>)
        }
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  collection: (state, ownProps) => selectCollection(ownProps.match.params.collectionId)(state),
  searchField: selectSearchField
});

export default connect(mapStateToProps)(CollectionPage);