const INITIAL_STATE = {
    sections : [
        {
          title: 'appetizer',
          imageUrl: require('../../assets/image-url/spring-roll.webp'),
          id: 1,
          linkUrl: 'shop/appetizer'
        },
        {
          title: 'dish',
          imageUrl: require('../../assets/image-url/bun-cha.webp'),
          id: 2,
          linkUrl: 'shop/dish'
        },
        {
          title: 'dessert',
          imageUrl: require('../../assets/image-url/egg-coffee.webp'),
          id: 3,
          linkUrl: 'shop/dessert'
        },
        {
            title: 'drink',
            imageUrl: require('../../assets/image-url/drink.webp'),
            size: 'large',
            id: 4,
            linkUrl: 'shop/drink'
        },
        {
            title: 'liquor',
            imageUrl: require('../../assets/image-url/liquor.webp'),
            size: 'large',
            id: 5,
            linkUrl: 'shop/liquor'
        },
    ]
};

const directoryReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        default:
            return state;
    }
}

export default directoryReducer