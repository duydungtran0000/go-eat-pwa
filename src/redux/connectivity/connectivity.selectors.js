import { createSelector } from 'reselect';

const selectConnectivity = state => state.connectivity;

export const selectConnecting = createSelector(
    [selectConnectivity],
    connectivity => connectivity.isConnecting
  );