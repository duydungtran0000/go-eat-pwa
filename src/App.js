import React, { lazy, Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { Offline, Online } from "react-detect-offline";

import { selectCurrentUser } from "./redux/user/user.selectors";
import { checkUserSession } from "./redux/user/user.actions";

import Spinner from "./components/spinner/spinner.components";
import Header from "./components/header/header.component";
import ErrorBoundary from "./components/error-boundary/error-boundary.component";
import AlertDialogSlide from "./components/alert-dialog-slide/alert-dialog-slide.component";
import ReactNotification from "react-notifications-component";

import "react-notifications-component/dist/theme.css";

import { GlobalStyle } from "./global.styles";

const HomePage = lazy(() => import("./pages/home/home-page.component"));
const ShopPage = lazy(() => import("./pages/shop/shop.component"));
const ProfilePage = lazy(() => import("./pages/profile/profile.component"));
const ContactPage = lazy(() => import("./pages/contact/contact.component"));
const SignInAndSignUpPage = lazy(() =>
  import("./pages/sign-in-and-sign-up/sign-in-and-sign-up-page.component")
);
const CheckoutPage = lazy(() => import("./pages/checkout/checkout.component"));
const FavouritePage = lazy(() =>
  import("./pages/favourite/favourite.component")
);
const OrderPage = lazy(() => import("./pages/order/order.component"));

class App extends React.Component {
  unsubscribeFromAuth = null;

  componentDidMount() {
    const { checkUserSession } = this.props;
    checkUserSession();
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }

  render() {
    return (
      <div>
        <ReactNotification />
        <Offline>
          <GlobalStyle color="Offline" />
          <AlertDialogSlide status={false}>
            Disconnected. Check your conection!!
          </AlertDialogSlide>
        </Offline>
        <Online>
          <GlobalStyle />
          <AlertDialogSlide status={true}>Connected</AlertDialogSlide>
        </Online>
        <Header />
        <Switch>
          <ErrorBoundary>
            <Suspense fallback={<Spinner />}>
              <Route exact path="/" component={HomePage} />
              <Route path="/shop" component={ShopPage} />
              <Route exact path="/checkout" component={CheckoutPage} />
              <Route
                exact
                path="/signin"
                render={() =>
                  this.props.currentUser ? (
                    <Redirect to="/" />
                  ) : (
                    <SignInAndSignUpPage />
                  )
                }
              />
              <Route
                exact
                path="/profile"
                render={() =>
                  this.props.currentUser ? <ProfilePage /> : <Redirect to="/" />
                }
              />
              <Route
                path="/favourite"
                render={() =>
                  this.props.currentUser ? (
                    <FavouritePage />
                  ) : (
                    <Redirect to="/" />
                  )
                }
              />
              <Route
                path="/order"
                render={() =>
                  this.props.currentUser ? <OrderPage /> : <Redirect to="/" />
                }
              />
              <Route path="/contact" component={ContactPage} />
            </Suspense>
          </ErrorBoundary>
        </Switch>
      </div>
    );
  }
}

//get state
const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  checkUserSession: () => dispatch(checkUserSession()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
