import React from "react";
import Popup from "reactjs-popup";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import Rating from "@material-ui/lab/Rating";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

import { selectOrderUser } from "../../redux/order/order.selectors";

import CustomButton from "../custom-button/custom-button.component";

import { setOrder } from "../../redux/order/order.actions";

import "./popup.styles.scss";

import { updateCollections } from "../../firebase/firebase.utils";
import { getObjectToAdd } from "./popup.utils";
import { selectCollections } from "../../redux/shop/shop.selectors";
import { setCollections } from "../../redux/shop/shop.actions";

class PopupComponent extends React.Component {
  constructor() {
    super();

    this.state = {
      value: 0,
      comment: "",
      visible: false
    };
  }

  handleValue = (e) => {
    this.setState({ value: parseInt(e.target.value) });
  };

  handleComment = (e) => {
    this.setState({ comment: e.target.value });
  };

  send = () => {
    const { id, collections } = this.props;
    var objectToAdd = getObjectToAdd(collections, this.state, id)
    setCollections(objectToAdd.collections);
    updateCollections(objectToAdd.collectionsToAdd, objectToAdd.docId, objectToAdd.establishment);
  };

  render() {
    return (
      <Popup visible={this.state.visible} trigger={<button className="edit-button">&#9998;</button>} modal>
        {(close) => (
          <div className="modal">
            <div className="header"> REVIEW </div>
            <div className="content">
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Rate</Typography>
                <Rating
                  name="simple-controlled"
                  value={this.state.value}
                  onChange={this.handleValue}
                />
              </Box>
              <TextareaAutosize
                type="text"
                aria-label="minimum height"
                rowsMin={3}
                placeholder="Minimum 3 rows"
                onChange={this.handleComment}
              />
            </div>
            <div className="actions">
              <CustomButton onClick={() => { this.send(); close();}}>SEND</CustomButton>
              <CustomButton
                onClick={() => {
                  close();
                }}
              >
                Cancel
              </CustomButton>
            </div>
          </div>
        )}
      </Popup>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  orderUser: selectOrderUser,
  collections: selectCollections,
});

const mapDispatchToProps = (dispatch) => ({
  setOrder: (orderUser) => dispatch(setOrder(orderUser)),
  setCollections: (collections) => dispatch(setCollections(collections)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PopupComponent);
