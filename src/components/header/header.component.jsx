import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import CartIcon from "../cart-icon/cart-icon.component";
import CartDropdown from "../cart-dropdown/cart-dropdown.component";
import SearchBox from "../search-box/search-box.component";

import { setSearchField } from "../../redux/search/search.actions";

import { selectCartHidden } from "../../redux/cart/cart.selectors";
import { selectCurrentUser } from "../../redux/user/user.selectors";
import { selectSearchField } from "../../redux/search/search.selectors";
import AvatarAndMenuComponent from "../avatar-and-menu/avatar-and-menu.components";

import "./header.styles.scss";

const Header = ({ currentUser, hidden, searchField, setSearchField }) => {

  let handleChange = (e) => {
    searchField = e.target.value;
    setSearchField(searchField);
  };

  return (
    <div className="header">
      <Link className="logo-container" to="/">
      <h1>Go Eat</h1>
      </Link>
      <div className="options">
        <SearchBox
          placeholder="Search restaurant"
          handleChange={handleChange}
        />
        <Link className="option" to="/shop">
          SHOP
        </Link>
        <Link className="option" to="/contact">
          CONTACT
        </Link>
        {currentUser ? (
          <div>
            <AvatarAndMenuComponent/>
          </div>
        ) : (
          <Link className="option" to="/signin">
            SIGN IN
          </Link>
        )}
        <CartIcon />
      </div>
      {hidden ? null : <CartDropdown />}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setSearchField: (search) => dispatch(setSearchField(search)),
});

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  hidden: selectCartHidden,
  searchField: selectSearchField,
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);