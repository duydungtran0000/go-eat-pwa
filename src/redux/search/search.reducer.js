import { SearchActionTypes } from './search.types';

const INITIAL_STATE = {
    searchField: '',
    searchHidden: false
};

const searchReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SearchActionTypes.TOGGLE_SEARCH_HIDDEN:
      return {
          ...state,
          hidden: !state.hidden
    };
    case SearchActionTypes.SET_SEARCH_FIELD: 
    return {
      ...state,
      searchField: action.payload
    };
    default:
      return state;
  }
};

export default searchReducer;