import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import userReducer from './user/user.reducer';
import cartReducer from './cart/cart.reducer';
import directoryReducer from './directory/directory.reducer';
import shopReducer from './shop/shop.reducer';
import searchReducer from './search/search.reducer';
import favouriteReducer from './favourite/favourite.reducer';
import orderReducer from './order/order.reducer';
import connectivityReducer from './connectivity/connectivity.reducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['cart','favourite', 'user', 'shop']
};

const rootReducer = combineReducers({
  user: userReducer,
  cart: cartReducer,
  directory: directoryReducer,
  shop: shopReducer,
  search: searchReducer,
  favourite: favouriteReducer,
  order: orderReducer,
  connectivity: connectivityReducer
});

export default persistReducer(persistConfig, rootReducer);