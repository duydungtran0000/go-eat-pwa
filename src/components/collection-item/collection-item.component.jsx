import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { LazyLoadImage } from "react-lazy-load-image-component";

import { addItem } from "../../redux/cart/cart.actions";
import {
  addFavourite,
  clearItemFromFavourite,
} from "../../redux/favourite/favourite.actions";

import { selectFavouriteItems } from "../../redux/favourite/favourite.selectors";
import { selectCurrentUser } from "../../redux/user/user.selectors";

import { updateFavourite } from "../../firebase/firebase.utils";

import CustomButton from "../custom-button/custom-button.component";
import RateItem from "../rate-item/rate-item.component";

import "./collection-item.styles.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const CollectionItem = ({
  item,
  addItem,
  addFavourite,
  clearItemFromFavourite,
  removeSpan,
  favouriteItems,
  currentUser,
}) => {
  const { name, price, imageUrl, establishment, rate, comments } = item;

  function updateItemToFavourite(item, type) {
    if (currentUser) {
      switch (type) {
        case "add": {
          addFavourite(item);
          let objectsToAdd = {
            customerEmail: currentUser.email,
            orderItems: favouriteItems,
          };
          updateFavourite(objectsToAdd, currentUser);
          break;
        }
        case "remove": {
          clearItemFromFavourite(item);
          let objectsToAdd = {
            customerEmail: currentUser.email,
            orderItems: favouriteItems,
          };
          updateFavourite(objectsToAdd, currentUser);
          break;
        }
        default:
          break;
      }
    } else {
      toast.warn('You must login', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        });
    }
  }

  return (
    <div className="collection-item">
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div className="image">
        <LazyLoadImage
          effect="opacity"
          alt={name}
          src={imageUrl}
          height="305px"
          width="100%"
        />
      </div>

      <div className="collection-footer-1">
        <span className="name">{name}</span>
        <span className="price">{price}€</span>
      </div>
      <div className="collection-footer-2">
        <span className="establishment">By {establishment}</span>
        {removeSpan ? (
          <span
            className="favoris"
            onClick={() => updateItemToFavourite(item, "remove")}
          >
            Remove item
          </span>
        ) : (
          <span
            className="favoris"
            onClick={() => updateItemToFavourite(item, "add")}
          >
            Favoris
          </span>
        )}
        <RateItem rate={rate} comments={comments}></RateItem>
      </div>
      <CustomButton onClick={() => addItem(item)} inverted>
        Add to cart
      </CustomButton>
    </div>
  );
};

const mapStateToprops = createStructuredSelector({
  favouriteItems: selectFavouriteItems,
  currentUser: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  addItem: (item) => dispatch(addItem(item)),
  addFavourite: (item) => dispatch(addFavourite(item)),
  clearItemFromFavourite: (item) => dispatch(clearItemFromFavourite(item)),
});

export default connect(mapStateToprops, mapDispatchToProps)(CollectionItem);
