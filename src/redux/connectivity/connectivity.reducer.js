import { ConnectivityActionTypes } from './connectivity.types';

const INITIAL_STATE = {
    isConnecting: null
};

const connectivityReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ConnectivityActionTypes.SET_CONNECTIVITY:
            return {
                ...state,
                isConnecting: action.payload
            }
        default:
            return state
    }
};

export default connectivityReducer;