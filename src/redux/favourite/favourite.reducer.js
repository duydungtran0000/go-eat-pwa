import { FavouriteActionTypes } from "./favourite.types";
import { addItemToFavourite } from "./favourite.utils";

const INITIAL_STATE = {
  favouriteItems: [],
  isFetching: false,
  errorMessage: undefined
};

const favouriteReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FavouriteActionTypes.SET_FAVOURITE_ITEMS:
      return {
        ...state,
        favouriteItems: action.payload
      }
    case FavouriteActionTypes.ADD_FAVOURITE:
      return {
        ...state,
        favouriteItems: addItemToFavourite(
          state.favouriteItems,
          action.payload
        ),
      };
    case FavouriteActionTypes.CLEAR_ITEM_FROM_FAVOURITE:
      return {
        ...state,
        favouriteItems: state.favouriteItems.filter(
          (favouriteItem) => favouriteItem.id !== action.payload.id
        ),
      };
    case FavouriteActionTypes.FETCH_FAVOURITE_START:
      return {
        ...state,
        isFetching: true
      }
    case FavouriteActionTypes.FETCH_FAVOURITE_SUCCESS:
        return {
          ...state,
          isFetching: false
        }
    case FavouriteActionTypes.FETCH_FAVOURITE_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};

export default favouriteReducer;
