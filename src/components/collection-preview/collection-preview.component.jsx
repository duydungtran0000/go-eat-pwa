import React from "react";
import { withRouter } from "react-router-dom";
import "./collection-preview.styles.scss";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectSearchField } from "../../redux/search/search.selectors";

import CollectionItem from "../collection-item/collection-item.component";

const CollectionPreview = ({ title, items, history, match, routeName, searchField }) => {
  return (
    <div className="collection-preview">
      <h1
        className="title"
        onClick={() => history.push(`${match.path}/${routeName}`)}
      >
        {title.toUpperCase()}
      </h1>
      <div className="preview">
        {items
          .filter((item, idx) => idx < 4)
          .filter(item => item.establishment.toLowerCase().includes(searchField.toLowerCase()))
          .map((item) => (
            <CollectionItem key={item.id} item={item} />
          ))}
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  searchField: selectSearchField
});

export default connect(mapStateToProps)(withRouter(CollectionPreview));
