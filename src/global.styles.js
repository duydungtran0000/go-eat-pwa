import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    body {
        font-family: 'Open Sans Condensed';
        padding: 20px 80px;
        background-color:  ${props => {
            switch (props.color) {
                  case "Offline":
                      return "#FA8072";
                  default:
                       return "transparent";
          }
         }};
        
        @media screen and (max-width: 800px) {
            padding: 10px 0px
        }
    }
    
    a {
        text-decoration: none;
        color: black;
    }
    
    * {
        box-sizing: border-box;
    }
`
