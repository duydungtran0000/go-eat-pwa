import React from "react";
import Popup from "reactjs-popup";

import "./rate-item.styles.scss";

const contentStyle = {
  maxWidth: "600px",
  width: "90%",
};

const RateItem = ({ rate, comments }) => {
  function getSum(arr) {
    if (!Array.isArray(arr)) return;
    let sum = arr.reduce((a, v) => a + v);
    let avgRate = sum / (rate.length - 1);
    return parseFloat(avgRate.toFixed(1));
  }

  return (
    <Popup
      trigger={
        rate.length <= 1 ? (
          <span className="rate">
            Rate:
            {rate}/5
          </span>
        ) : (
          <span className="rate">
            Rate:
            {getSum(rate)}/5
          </span>
        )
      }
      modal
      contentStyle={contentStyle}
    >
      {(close) => (
        <div className="modal">
          <button className="close" onClick={close}>
            &times;
          </button>
          <div className="header"> COMMENT </div>
          <div className="content">
            {comments.map((comment) => (
                <div className="comment">{comment}</div>
            ))}
          </div>
        </div>
      )}
    </Popup>
  );
};

export default RateItem;
