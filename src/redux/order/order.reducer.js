import { OrderActionTypes } from './order.types';

const INITIAL_STATE = {
    orderUser: []
  };

const orderReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case OrderActionTypes.SET_ORDER:
            return {
                ...state,
                orderUser: action.payload
            };
        default:
            return state;
    }
}

export default orderReducer;