export const addItemToFavourite = (favouriteItems, favouriteItemToAdd) => {
    const existingCartItem = favouriteItems.find(
      (cartItem) => cartItem.id === favouriteItemToAdd.id
    );
  
    if (existingCartItem) {
      return favouriteItems.map((favouriteItem) =>
      favouriteItem.id === favouriteItemToAdd.id
          ? { ...favouriteItem, quantity: 1 }
          : favouriteItem
      );
    }
  
    return [...favouriteItems, { ...favouriteItemToAdd, quantity: 1 }];
  };

  export const removeItemFromFavourite = (favouriteItems, favouriteItemToRemove) => {
    const existingCartItem = favouriteItems.find(
      (cartItem) => cartItem.id === favouriteItemToRemove.id
    );
  
    if (existingCartItem.quantity === 1) {
      return favouriteItems.filter((favouriteItem) => favouriteItem.id !== favouriteItemToRemove.id);
    }
  
    return favouriteItems.map((favouriteItem) =>
    favouriteItem.id === favouriteItemToRemove.id
        ? { ...favouriteItem, quantity: favouriteItem.quantity - 1 }
        : favouriteItem
    );
  };