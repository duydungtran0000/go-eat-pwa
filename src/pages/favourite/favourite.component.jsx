import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectFavouriteItems } from "../../redux/favourite/favourite.selectors";
import { setFavouriteItems } from "../../redux/favourite/favourite.actions";
import { selectCurrentUser } from "../../redux/user/user.selectors";

import { firestore } from "../../firebase/firebase.utils";

import CollectionItem from "../../components/collection-item/collection-item.component";
import "./favourite.styles.scss";

class FavouritePage extends React.Component  {
  removeSpan = true;

  componentDidMount(){
    const { currentUser, setFavouriteItems} = this.props;
    const favouriteItem = firestore.collection("favourites").doc(`${currentUser.id}`)
    favouriteItem.get().then(function(doc) {
      const { orderItems } = doc.data();
      setFavouriteItems(orderItems)
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });  
  }
  
  render(){
    return (
      <div className="favourite-page">
        <h2 className="title"> Favourite </h2>
        <div className="items">
          {this.props.favouriteItems.map((item) => (
            <CollectionItem key={item.id} item={item} removeSpan={this.removeSpan}/>
          ))}
        </div>
      </div>
    );
  }
};

const mapStateToprops = createStructuredSelector({
  favouriteItems: selectFavouriteItems,
  currentUser: selectCurrentUser
});

const mapDispatchToProps = (dispatch) => ({
  setFavouriteItems: (item) => dispatch(setFavouriteItems(item)),
});

export default connect(mapStateToprops, mapDispatchToProps)(FavouritePage);
