import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import CollectionPreview from "../collection-preview/collection-preview.component";
import { selectCollectionsForPreview } from "../../redux/shop/shop.selectors";
import { setCollections } from "../../redux/shop/shop.actions";
import "./collections-overview.component";

class CollectionsOverview extends React.Component {
  render() {
    return (
      <div className="collections-overview">
        {this.props.collections.map(({ id, ...otherCollectionProps }) => (
          <CollectionPreview
            key={id}
            {...otherCollectionProps}
          ></CollectionPreview>
        ))}
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  collections: selectCollectionsForPreview,
});

const mapDispatchToProps = (dispatch) => ({
  setCollections: (collections) => dispatch(setCollections(collections)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionsOverview);
