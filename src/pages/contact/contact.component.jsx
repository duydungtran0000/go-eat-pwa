import React from "react";

import "./contact.styles.scss";

const ContactPage = () => (
  <div className="contact-page">
    <h2 className="title">Contact</h2>
    <div className="contact-content">
        <h5>duydungtran.0000@gmail.com</h5>
    </div>
  </div>
);

export default ContactPage;
