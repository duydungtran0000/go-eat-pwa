import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import "./profile.styles.scss";

import { selectCurrentUser } from "../../redux/user/user.selectors";

class ProfilePage extends React.Component {
  render() {
    const { currentUser } = this.props
    return (
      <div className="profile-page">
        <h2 className="title">Profile</h2>
        <div className="profile-content">
          <h5>Display name : {currentUser.displayName}</h5>
          <h5>Email : {currentUser.email}</h5>
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
  });

export default connect(mapStateToProps)(ProfilePage);
