import { SearchActionTypes } from './search.types';

export const setSearchField = search => ({
  type: SearchActionTypes.SET_SEARCH_FIELD,
  payload: search
});

export const toggleSearchHidden = () => ({
    type: SearchActionTypes.TOGGLE_SEARCH_HIDDEN
});