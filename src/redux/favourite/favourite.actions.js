import { FavouriteActionTypes } from './favourite.types';

import {
    firestore,
    convertCollectionsSnapshotToMap
} from '../../firebase/firebase.utils';

export const setFavouriteItems = item => ({
    type:  FavouriteActionTypes.SET_FAVOURITE_ITEMS,
    payload: item
})

export const addFavourite = item => ({
    type:  FavouriteActionTypes.ADD_FAVOURITE,
    payload: item
})

export const clearItemFromFavourite = item => ({
    type:  FavouriteActionTypes.CLEAR_ITEM_FROM_FAVOURITE,
    payload: item
})

export const fetchFavouriteStart = () => ({
    type: FavouriteActionTypes.FETCH_FAVOURITE_START
  });

  export const fetchFavouriteSuccess = favouriteMap => ({
    type: FavouriteActionTypes.FETCH_FAVOURITE_SUCCESS,
    payload: favouriteMap
  });

  export const fetchFavouriteFailure = errorMessage => ({
    type: FavouriteActionTypes.FETCH_FAVOURITE_FAILURE,
    payload: errorMessage
  });

  export const fetchFavouriteAsync = (currentUser) => {
    return dispatch => {
      const collectionRef = firestore.collection("favourites").doc(`${currentUser.id}`);
      // call function fetchCollectionStart and swicth FETCH_COLLECTIONS_START in reducer
      dispatch(fetchFavouriteStart());
  
      collectionRef
        .get()
        .then(snapshot => {
          const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
          dispatch(fetchFavouriteSuccess(collectionsMap));
        })
        .catch(error => dispatch(fetchFavouriteFailure(error.message)));
    };
  };