import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import {
  selectCartItems,
  selectCartTotal,
} from "../../redux/cart/cart.selectors";

import { clearCart } from "../../redux/cart/cart.actions";

import { selectCurrentUser } from "../../redux/user/user.selectors";

import CheckoutItem from "../../components/checkout-item/checkout-item.component";

import CustomButton from "../../components/custom-button/custom-button.component";

import { addCollectionAndDocuments } from "../../firebase/firebase.utils";

import "./checkout.styles.scss";

const CheckoutPage = ({ cartItems, total, currentUser, clearCart }) => {
  
  function payOrder() {
    if (currentUser) {
      let createdAt = new Date();
      var objectsToAdd = {
        customerEmail: currentUser.email,
        customerDisplayName: currentUser.displayName,
        orderItems: cartItems,
        createdAt: createdAt,
        status: "Not valid",
      };
      try {
        addCollectionAndDocuments("order", objectsToAdd);
        clearCart();
      } catch (error) {
        toast.danger(error, {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      toast.warn("You must login", {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  return (
    <div className="checkout-page">
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <div className="checkout-header">
        <div className="header-block">
          <span>Product</span>
        </div>
        <div className="header-block">
          <span>Description</span>
        </div>
        <div className="header-block">
          <span>Quantity</span>
        </div>
        <div className="header-block">
          <span>Price</span>
        </div>
        <div className="header-block">
          <span>Establishment</span>
        </div>
        <div className="header-block">
          <span>Remove</span>
        </div>
      </div>
      {cartItems.map((cartItem) => (
        <CheckoutItem key={cartItem.id} cartItem={cartItem} />
      ))}
      <div className="total">
        <span>TOTAL: {total}€</span>
      </div>

      <CustomButton type="submit" onClick={payOrder}>
        Pay
      </CustomButton>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartItems,
  total: selectCartTotal,
  currentUser: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  clearCart: () => dispatch(clearCart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);
