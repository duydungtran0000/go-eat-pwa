import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import OrderItem from "../../components/order-item/order-item.component";

import { selectOrderUser } from "../../redux/order/order.selectors";
import { selectCurrentUser } from "../../redux/user/user.selectors";
import { setOrder } from "../../redux/order/order.actions";

import { firestore } from "../../firebase/firebase.utils";

import "./order.styles.scss";

class OrderPage extends React.Component {
  state = {
    loading: true,
  };

  componentDidMount() {
    const { currentUser, setOrder } = this.props;

    firestore
      .collection("order")
      .where("customerEmail", "==", currentUser.email)
      .orderBy("createdAt")
      .get()
      .then(function (querySnapshot) {
        const orderItems = [];
        querySnapshot.forEach(function (doc) {
          orderItems.push({
            docId: doc.id,
            createdAt: doc.data().createdAt.toDate(),
            orderItems: doc.data().orderItems,
            status: doc.data().status,
          });
        });
        setOrder(orderItems);
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  render() {
    return (
      <div className="order-page">
        <h2 className="title">YOUR ORDERS</h2>
        <div className="order-header">
          <div className="header-block">
            <span>Product</span>
          </div>
          <div className="header-block">
            <span>Description</span>
          </div>
          <div className="header-block">
            <span>Quantity</span>
          </div>
          <div className="header-block">
            <span>Price</span>
          </div>
          <div className="header-block">
            <span>Establishment</span>
          </div>
          <div className="header-block">
            <span>Review</span>
          </div>
        </div>
        {this.props.orderUser.map((order, i) => {
          return (
            <div className="order-body" key={i}>
              {order.orderItems.map((orderItem, i) => (
                <OrderItem key={orderItem.id} orderItem={orderItem} />
              ))}
              <div className="status">Status: {order.status}</div>
              <div className="date">Date: {order.createdAt.toString()}</div>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  orderUser: selectOrderUser,
});

const mapDispatchToProps = (dispatch) => ({
  setOrder: (orderUser) => dispatch(setOrder(orderUser)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderPage);
