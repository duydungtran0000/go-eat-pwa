import React from "react";

import "./search-box.styles.scss";

const SearchBox = ({ placeholder, handleChange }) => (
  <label className="search"> {placeholder}
    <input
      className="input-search"
      type="search"
      onChange={handleChange}
    />
  </label>
);

export default SearchBox;
