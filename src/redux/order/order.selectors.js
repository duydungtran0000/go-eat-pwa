import { createSelector } from "reselect";

const selectOrder = state => state.order;

export const selectOrderUser = createSelector(
    [selectOrder],
    order => order.orderUser
)