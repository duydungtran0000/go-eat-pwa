import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectOrderUser } from "../../redux/order/order.selectors";

import PopupComponent from "../popup/popup.component";

import "./order-item.styles.scss";

const OrderItem = ({ orderItem }) => {
  const { id, name, imageUrl, price, quantity, establishment } = orderItem;

  return (
    <div className="checkout-item">
      <div className="image-container">
        <img src={imageUrl} alt="item" />
      </div>
      <span className="name">{name}</span>
      <span className="quantity">
        <span className="value">{quantity}</span>
      </span>
      <span className="price">{price}</span>
      <span className="establishment">{establishment}</span>
      <PopupComponent id={id}/>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  orderUser: selectOrderUser,
});

export default connect(mapStateToProps)(OrderItem);
