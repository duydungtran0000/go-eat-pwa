import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import { store } from "react-notifications-component";

const firebaseConfig = {
  apiKey: "AIzaSyBw1_KDBCOo9zoDyCtPJaFTRi3yqSbKxQ8",
  authDomain: "go-eat-5c0ab.firebaseapp.com",
  databaseURL: "https://go-eat-5c0ab.firebaseio.com",
  projectId: "go-eat-5c0ab",
  storageBucket: "go-eat-5c0ab.appspot.com",
  messagingSenderId: "816160606887",
  appId: "1:816160606887:web:a5fac07c1c0330bd9ae982",
  measurementId: "G-7730JRSLYY",
};
firebase.initializeApp(firebaseConfig);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return; //null
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  
  const snapShot = await userRef.get();
  
  if (!snapShot.exists) {
    //false user not exist on firebase
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData,
      });
    } catch (error) {
      console.log("error creating user", error.message);
    }
  }

  return userRef;
};

export const addCollectionAndDocuments = async (
  collectionKey,
  objectsToAdd
) => {
  const collectionRef = firestore.collection(collectionKey);

  try {
    await collectionRef.add(objectsToAdd);

    if (collectionKey === "order") {
      store.addNotification({
        title: "Your order is on the way!",
        message: objectsToAdd.customerEmail,
        type: "success",
        insert: "top",
        container: "bottom-left",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
        },
        width: 600,
      });
    }
  } catch (error) {
    store.addNotification({
      title: "Error",
      message: "Please check your connection",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
      },
      width: 600,
    });
  }
};

export const updateFavourite = async (objectsToAdd, currentUser) => {
  var favouritesRef = firestore
    .collection("favourites")
    .doc(`${currentUser.id}`);

  try {
    const { customerEmail, orderItems } = objectsToAdd;
    await favouritesRef.set({
      customerEmail,
      orderItems,
    });
  } catch (error) {
    store.addNotification({
      title: "Error",
      message: "Updating collection in database",
      type: "danger",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
      },
      width: 600,
    });
  }
};

export const updateCollections = async (objectsToAdd, docId, establishment) => {
  var collectionsRef = firestore.collection("collections").doc(`${docId}`);
  try {
    const { items, title } = objectsToAdd;
    await collectionsRef.set({
      items,
      title,
    });

    store.addNotification({
      title: "Your review sent to",
      message: establishment,
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
      },
      width: 600,
    });
  } catch (error) {
    alert("Error updating collection in database");
  }
};

export const fetchFavourite = (currentUser) => {
  const favouriteItem = firestore
    .collection("favourites")
    .doc(`${currentUser.id}`);
  var data = null;
  favouriteItem
    .get()
    .then(function (doc) {
      if (doc.exists) {
        data = doc.data().orderItems;
      } else {
        data = [];
      }
    })
    .catch(function (error) {
      console.log("Error getting document:", error);
    });
  return data;
};

export const convertCollectionsSnapshotToMap = (collections) => {
  const transformedCollection = collections.docs.map((doc) => {
    const { title, items } = doc.data();

    return {
      routeName: encodeURI(title.toLowerCase()),
      id: doc.id,
      title,
      items,
    };
  });

  //this value is array
  // console.log(transformedCollection);

  //change array into object
  return transformedCollection.reduce((accumulator, collection) => {
    accumulator[collection.title.toLowerCase()] = collection;
    return accumulator;
  }, {});
};

export const convertFavouriteSnapshotToMap = (collections) => {
  const transformedFavouriteCollection = collections.docs.map((doc) => {
    const { customerEmail, orderItems } = doc.data();
    return {
      customerEmail,
      orderItems,
    };
  });
  //this value is array
  console.log(transformedFavouriteCollection);
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      unsubscribe()
      resolve(userAuth)
    }, reject)
  })
}

export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

// export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) =>{
//
//     const collectionRef = firestore.collection(collectionKey);
//
//      console.log(collectionRef)

//     const batch = firestore.batch();

//     objectsToAdd.forEach(obj => {
//         const newDocRef = collectionRef.doc();
//         return random id unique
//         batch.set(newDocRef, obj);
//     });

//     // batch fire off obj into fireStore, return from calling to commit is a promise
//     return await batch.commit()
// }

export default firebase;
