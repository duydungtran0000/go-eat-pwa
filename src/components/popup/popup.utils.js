export const getObjectToAdd = (collections, state, id) => {
    var collectionsToAdd = null
    var docId = null;
    var establishment = null
    if (collections) {
        collections.liquor.items.find((item) => {
          if (item.id === id) {
            docId = collections.liquor.id;
            establishment = item.establishment;
            item.rate.push(state.value);
            item.comments.push(state.comment)
            collectionsToAdd = collections.liquor
            return true;
          }
        });
        collections.dish.items.find((item) => {
          if (item.id === id) {
            docId = collections.dish.id;
            establishment = item.establishment
            item.rate.push(state.value);
            item.comments.push(state.comment)
            collectionsToAdd = collections.dish
            return true;
          }
        });
        collections.appetizer.items.find((item) => {
          if (item.id === id) {
            docId = collections.appetizer.id;
            establishment = item.establishment
            item.rate.push(state.value);
            item.comments.push(state.comment)
            collectionsToAdd = collections.appetizer
            return true;
          }
        });
        collections.dessert.items.find((item) => {
          if (item.id === id) {
            docId = collections.dessert.id;
            establishment = item.establishment
            item.rate.push(state.value);
            item.comments.push(state.comment);
            collectionsToAdd = collections.dessert
            return true;
          }
        });
        collections.drink.items.find((item) => {
          if (item.id === id) {
            docId = collections.drink.id;
            establishment = item.establishment
            item.rate.push(this.state.value);
            item.comments.push(this.state.comment);
            collectionsToAdd = collections.drink
            return true;
          }
        });
      } else {
        alert("collections is null");
      }

      var objectToAdd = {
        collectionsToAdd: collectionsToAdd,
        docId: docId,
        collections: collections,
        establishment: establishment
      }

      return objectToAdd
}