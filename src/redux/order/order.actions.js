import { OrderActionTypes } from './order.types';

export const setOrder = item => ({
    type: OrderActionTypes.SET_ORDER,
    payload: item
})