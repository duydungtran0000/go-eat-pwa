import { ConnectivityActionTypes } from './connectivity.types';

export const setConnectivity = isConnecting => ({
    type: ConnectivityActionTypes.SET_CONNECTIVITY,
    payload: isConnecting
});