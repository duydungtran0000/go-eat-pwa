import { createSelector } from 'reselect';

const selectFavourite = state => state.favourite;

export const selectFavouriteItems = createSelector(
    [selectFavourite],
    favourite => favourite.favouriteItems
  );

  export const selectIsFavouriteFetching = createSelector(
    [selectFavourite],
    (favourite) => favourite.isFetching
  );